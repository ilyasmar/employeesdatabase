# Employee database Test task

Web application for adding and searching employees using Spring Boot Thymeleaf, Data JPA, JQuery.

### Run the project:
- Make sure to be in the root directory
- Need to start the mysql database with configuration username:root, password:root
(or change the data in src/main/resources/application.properties).
- Clean and build the project, run the command:
```aidl
mvn clean spring-boot:run
```
- The application starts and opens the page in the browser: http://localhost:8080/employees
