package testTasks.employeesDatabase.controllers;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import testTasks.employeesDatabase.exceptions.EmployeeNotFoundException;
import testTasks.employeesDatabase.models.City;
import testTasks.employeesDatabase.models.Employee;
import testTasks.employeesDatabase.services.CountryService;
import testTasks.employeesDatabase.services.EmployeeService;


import java.util.List;

@Controller
public class EmployeeController {


    @Autowired
    private CountryService countryService;
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    public String allEmployeesForm(Model model, @Param("keyword") String keyword) {
        List<Employee> employeesList = employeeService.listAllEmployees(keyword);
        model.addAttribute("employeesList", employeesList);
        model.addAttribute("keyword", keyword);
        return "employees";
    }

    @GetMapping("/new-employee")
    public String newEmployeeForm(Model model) {
        model.addAttribute("countries", countryService.getAllCountries());
        model.addAttribute("employee", new Employee());
        return "new-employee";
    }

    @PostMapping("/new-employee")
    String newEmployee(@ModelAttribute("employee") @Valid Employee employee, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("countries", countryService.getAllCountries());
            return "new-employee";
        }
        employeeService.save(employee);
        return "redirect:/employees";
    }

    @GetMapping("/employees/{id}")
    Employee getEmployee(@PathVariable Long id) {

        return employeeService.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }


    @DeleteMapping("/employees/{id}")
    void deleteEmployee(@PathVariable Long id) {
        employeeService.deleteById(id);
    }

    @GetMapping("/employees/all")
    List<Employee> allEmployees() {
        return employeeService.findAll();
    }

    @GetMapping("/cities")
    public @ResponseBody List<City> getAllCitiesByCountryId(@RequestParam Long id)
    {
        return countryService.getCitiesByCountry(id);
    }

}
