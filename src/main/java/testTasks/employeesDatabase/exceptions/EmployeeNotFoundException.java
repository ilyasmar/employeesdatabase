package testTasks.employeesDatabase.exceptions;

public class EmployeeNotFoundException extends RuntimeException {

    public EmployeeNotFoundException(Long id) {
        super("Не найден сотрудник № " + id);
    }
}