package testTasks.employeesDatabase.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "cities")
@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class City {

    @Id
    @NonNull
    private Integer id;

    @Column(name = "name")
    private @NonNull String name;

}