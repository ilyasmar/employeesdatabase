package testTasks.employeesDatabase.models;

import jakarta.persistence.*;
import lombok.*;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.Set;


@Entity
@Table(name = "countries")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Country  {

    @Id
    @NonNull
    private Integer id;

    @Column(name = "name", unique = true)
    private @NonNull String name;

    @OneToMany
    @JoinColumn(name="country_id")
    private Set<City> cities;

}