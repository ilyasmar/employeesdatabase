package testTasks.employeesDatabase.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;


@Entity
@Table(name="employees")

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min=2, max=30, message = "Неверная длина фамилии")
    @NotEmpty(message = "Поле фамилия не может быть пустым")
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ]+([\\s-][a-zA-Zа-яА-ЯёЁ]+)*$", message = "Фамилия должна содержать только буквы")
    private String lastName;

    @NonNull
    @Size(min=2, max=30, message = "Неверная длина имени")
    @NotEmpty(message = "Поле имя не может быть пустым")
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ]+([\\s-][a-zA-Zа-яА-ЯёЁ]+)*$", message = "Имя должно содержать только буквы")
    private String firstName;

    @NonNull
    @Size(min=2, max=30, message = "Неверная длина отчества")
    @NotEmpty(message = "Поле отчество не может быть пустым")
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ]+([\\s-][a-zA-Zа-яА-ЯёЁ]+)*$", message = "Отчество должно содержать только буквы")
    private String middleName;

    @NonNull
    @Email(message = "Неверный формат email")
    @NotEmpty(message = "Поле email не может быть пустым")
    private String email;

    @NonNull
    @Pattern(regexp="^\\+?[0-9]{1,3}[0-9]{1,12}$", message="Неправильный формат телефонного номера")
    @NotEmpty(message = "Поле телефонный номер не может быть пустым")
    private String phoneNumber;

    @JoinColumn(name = "country_id", referencedColumnName = "id")
    @ManyToOne
    @NonNull
    @NotNull(message = "Выберите страну проживания")
    private Country country;

    @JoinColumn(name = "city_id", referencedColumnName = "id")
    @ManyToOne
    @NonNull
    @NotNull(message = "Выберите город проживания")
    private City city;

}
