package testTasks.employeesDatabase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import testTasks.employeesDatabase.models.City;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

}
