package testTasks.employeesDatabase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import testTasks.employeesDatabase.models.City;
import testTasks.employeesDatabase.models.Country;

import java.util.List;

public interface CountryRepository extends JpaRepository<Country, Long> {

    @Query("SELECT co.cities FROM Country co WHERE co.id=:id")
    List<City> getCitiesByCountry(Long id);
}
