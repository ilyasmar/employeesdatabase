package testTasks.employeesDatabase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import testTasks.employeesDatabase.models.City;
import testTasks.employeesDatabase.models.Employee;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query("SELECT e  FROM Employee e WHERE CONCAT(e.lastName, ' ', e.firstName, ' ',e.middleName, ' ', e.email, ' ', e.phoneNumber, ' ', e.country.name, ' ', e.city.name) LIKE %?1%")
    List<Employee> getEmployeesByKeyword(String Keyword);

}
