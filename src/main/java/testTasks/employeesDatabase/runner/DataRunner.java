package testTasks.employeesDatabase.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import testTasks.employeesDatabase.models.City;
import testTasks.employeesDatabase.models.Country;
import testTasks.employeesDatabase.models.Employee;
import testTasks.employeesDatabase.repositories.CityRepository;
import testTasks.employeesDatabase.repositories.CountryRepository;
import testTasks.employeesDatabase.repositories.EmployeeRepository;

import java.util.Arrays;
import java.util.Set;

@Component
public class DataRunner implements CommandLineRunner {

    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public DataRunner(CityRepository cityRepository, CountryRepository countryRepository, EmployeeRepository employeeRepository) {
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public void run(String... args) {

        City city1 = new City(1, "Алматы");
        City city2 = new City(2, "Астана");
        City city3 = new City(3, "Актобе");
        City city4 = new City(4, "Шымкент");
        City city5 = new City(5, "Караганда");

        City city6 = new City(6, "Пекин");
        City city7 = new City(7, "Тяньцзинь");
        City city8 = new City(8, "Чунцин");
        City city9 = new City(9, "Шанхай");
        City city10 = new City(10, "Нанкин");

        City city11 = new City(11, "Лиссабон");
        City city12 = new City(12, "Порту");
        City city13 = new City(13, "Коимбра");
        City city14 = new City(14, "Брага");
        City city15 = new City(15, "Эвора");

        City city16 = new City(16, "Нью-Йорк");
        City city17 = new City(17, "Чикаго");
        City city18 = new City(18, "Лос-Анджелес");
        City city19 = new City(19, "Сан-Франциск");
        City city20 = new City(20, "Сиэтл");

        City city21 = new City(21, "Париж");
        City city22 = new City(22, "Старсбург");
        City city23 = new City(23, "Бордо");
        City city24 = new City(24, "Марсель");
        City city25 = new City(25, "Тулуза");

        if (cityRepository.count() == 0) {
            cityRepository.saveAll(Arrays.asList(city1, city2, city3, city4, city5, city6, city7, city8, city9, city10, city11, city12,
                    city13, city14, city15, city16, city17, city18, city19, city20, city21, city22, city23, city24, city25));
        }

        Country country1 = new Country(1, "Казахстан", Set.of(city1,city2,city3,city4,city5));
        Country country2 = new Country(2, "Китай", Set.of(city6,city7,city8,city9,city10));
        Country country3 = new Country(3, "Португалия", Set.of(city11,city12,city13,city14,city15));
        Country country4 = new Country(4, "США", Set.of(city16,city17,city18,city19,city20));
        Country country5 = new Country(5, "Франция", Set.of(city21,city22,city23,city24,city25));

        if (countryRepository.count() == 0) {
            countryRepository.saveAll(Arrays.asList(country1, country2, country3, country4, country5));
        }

            Employee employee1 = new Employee("Иванов", "Иван", "Иванович", "anymail@gmail.com", "+777777777", country1, city3);
            Employee employee2 = new Employee("Вайт", "Джон", "Экзамплович", "second@mail.ua", "921312312", country3, city12);


        if (employeeRepository.count() == 0) {
            employeeRepository.saveAll((Arrays.asList(employee1, employee2)));
        }

    }
}
