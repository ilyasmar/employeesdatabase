package testTasks.employeesDatabase.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import testTasks.employeesDatabase.models.City;
import testTasks.employeesDatabase.repositories.CityRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CityService {

    private final CityRepository cityRepository;

    public List<City> getAllCtiies(){
        return cityRepository.findAll();
    }


}

