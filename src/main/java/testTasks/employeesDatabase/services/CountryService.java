package testTasks.employeesDatabase.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import testTasks.employeesDatabase.models.City;
import testTasks.employeesDatabase.models.Country;
import testTasks.employeesDatabase.repositories.CountryRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CountryService {

    private final CountryRepository countryRepository;

    public List<Country> getAllCountries(){
        return countryRepository.findAll();
    }

    public List<City> getCitiesByCountry(Long id){
        return countryRepository.getCitiesByCountry(id);
    }
}

