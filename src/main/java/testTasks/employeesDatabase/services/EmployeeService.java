package testTasks.employeesDatabase.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import testTasks.employeesDatabase.models.Employee;
import testTasks.employeesDatabase.repositories.EmployeeRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public List<Employee> listAllEmployees(String keyword){
        if (keyword != null) {
            return employeeRepository.getEmployeesByKeyword(keyword);
        }
        return employeeRepository.findAll();
    }

    public List<Employee> findAll(){
        return employeeRepository.findAll();
    }

    public Optional<Employee> findById(Long id) {
        return employeeRepository.findById(id);
    }

    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }
}
