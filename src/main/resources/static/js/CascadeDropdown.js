$(document).ready(function() {
    $("#countriesList").change(function() {
        var id = $(this).val();
        var option = '';
        if (id > 0) {
            $.ajax({
                url : 'cities',
                data : { "id" : id },
                success : function(data) {
                    for (var i=0; i<data.length; i++){
                        option = option + "<option value='"+ data[i].id + "'>"+ data[i].name + "</option>";
                    }
                    $('#citiesList').html(option);
                }
            });
        }

        $('#citiesList').html(option);
    });
});